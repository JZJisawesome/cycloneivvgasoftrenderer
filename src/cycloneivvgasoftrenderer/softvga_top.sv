module softvga_top
(
	input logic clock,//Should be 25.175 mhz
	input logic reset,
	
	//VGA Outputs
	softvga_vga_if.out vgaIF//,
	
	//Todo maybe pins for buttons?
);
softvga_vgafb_if fbIF();

softvga_vga (.*);

endmodule : softvga_top